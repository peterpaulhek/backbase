/*
 Module      : index.js
 Description : This is the router for the application; only two pages: welcome and weather overview
 History     : 2017-07-06 PPH Created
 */

'use strict';

(function() {

  // Inject all dependencies
  angular.module('backbase', [
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'ngSanitize',
    'chart.js'
  ]);

}());

(function(module) {

  module.config(router);

  function router($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('welcome', {
        url: '/',
        templateUrl: 'app/welcome/welcome.html'
      })
      .state('overview', {
        url: '/overview',
        templateUrl: 'app/overview/overview.html'
      })
    ;

    $urlRouterProvider.otherwise('/');

  }

}(angular.module('backbase')));

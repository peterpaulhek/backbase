/*
 Module      : welcome.js
 Html        : welcome.html
 Description : The welcome page of the app
 History     : 2017-07-06 PPH Created
 */

'use strict';

(function (module) {

    module.controller('WelcomeCtrl', welcomeCtrl);

    function welcomeCtrl($location) {

        var model = this;

        model.continueToMainOverview = continueToMainOverview; // Callback of click event of the Continue button

        model.title = 'Welcome to the<br>Backbase Weather & Forecast';

        model.captionContinueToMainOverview = 'Continue';

        /**
         * Continue to the main overview
         */
        function continueToMainOverview() {
            $location.path('/overview');
        }
    }

}(angular.module('backbase')));

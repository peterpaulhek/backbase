angular.module('backbase')
    .constant('Config', {
        "cities":"Amsterdam,London,Paris,Madrid,Prague",
        "base": "http://api.openweathermap.org/data/2.5/",
        "weather": "weather",
        "forecast": "forecast",
        "appid": "appid=3d8b309701a13f65b660fa2c64cdc517"
    });

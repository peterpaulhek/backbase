/*
 Module      : overview.js
 Html        : overview.html
 Description : The overview page of the app
 History     : 2017-07-06 PPH Created
 */

'use strict';

(function (module) {

    module.controller('OverviewCtrl', overviewCtrl);

    function overviewCtrl($scope, Modal, API) {

        var model = this;

        // General purpose modal dialog
        var modal = new Modal.Modal($scope);

        model.title = 'Backbase Weather & Forecast';

        model.onRefreshWeather = onRefreshWeather; // Callback of click event of the Refresh button
        model.onRemoveWeather = onRemoveWeather; // Callback of click event of the Remove button
        model.onShowForecast = onShowForecast; // Callback of click event of the weather panel

        model.cities = []; // No data about the overview yet

        model.forecast  = {}; // No forecast yet

        function City(name) {
            this.name = name;
            this.isLoadingWeather = undefined;
            this.keyValuePairs = [];
            this.error = undefined;
        }

        // This exercise sets the display unit to metric; in real this should be user defined settings
        API.setDisplayUnit(API.displayUnits.metric);

        // A number of decimals of 1 seems reasonable
        API.setDisplayPrecision(1);

        /**
         * Loads the initial weather about cities and show a spinner to inform the user the weather is being loaded
         */
        function loadInitialWeather() {

            // Get the static data about the cities and mark as not yet being loaded
            angular.forEach(API.getCities(), function(city){
                model.cities.push(new City(city));
            });

            // Get the weather for all cities
            angular.forEach(model.cities, function(city, index){

                // Show  the spinner to inform the user the weather is being loaded
                city.isLoadingWeather = true;

                // Query the API for city weather
                API.getWeatherByName(city.name).then(function(data){

                    model.cities[index].keyValuePairs = [
                        {
                            key: 'Temperature',
                            value: data.averageTemparature
                        },
                        {
                            key: 'Wind Speed',
                            value: data.windSpeed
                        }
                    ];

                }, function(error){

                    model.cities[index].error = error;

                }).finally(function(){

                    // Hide the spinner (even after an error)
                    city.isLoadingWeather = false;
                });
            });
        }

        /**
         * Refreshes the weather for a specific city.
         */
        function onRefreshWeather(cityToRefresh) {

            // Find the city to refresh
            var index = _.findIndex(model.cities, function(city){return city.name === cityToRefresh;});

            // Reset any previous error
            model.cities[index].error = '';

            // Show  the spinner to inform the user the weather is being loaded
            model.cities[index].isLoadingWeather = true;

            // Query the API for city weather
            API.getWeatherByName(model.cities[index].name).then(function(data){

                model.cities[index].keyValuePairs = [
                    {
                        key: 'Temperature',
                        value: data.averageTemparature
                    },
                    {
                        key: 'Wind Speed',
                        value: data.windSpeed
                    }
                ];

            }, function(error){

                model.cities[index].error = error;

            }).finally(function(){

                // Hide the spinner (even after an error)
                model.cities[index].isLoadingWeather = false;
            });
        }

        /**
         * Removes the weather from a specific city
         */
        function onRemoveWeather(cityToRemove) {

            // Find the city to remove
            var index = _.findIndex(model.cities, function(city){return city.name === cityToRemove;});

            // Remove the city
            model.cities.splice(index, 1);
        }

        /**
         * Shows the forecast for a specific city in a separate modal dialog
         */
        function onShowForecast(cityToForecast) {

            // Find the city to forecast
            var index = _.findIndex(model.cities, function(city){return city.name === cityToForecast;});

            // Show the city in the dialog
            model.forecast.name = cityToForecast;

            // Reset any previous error
            model.forecast.error = '';

            // Show a spinner in the dialog while forecast is being loaded
            model.forecast.isLoadingForecast = true;

            // Query the API for city forecast
            API.getForecastByName(model.cities[index].name).then(function(data){

                model.forecast.chartOptions.scales.yAxes[0].scaleLabel.labelString = 'Temperature ' + data.displayUnitOfTemperature;
                model.forecast.chartOptions.scales.yAxes[1].scaleLabel.labelString = 'Wind Speed ' + data.displayUnitOfWind;

                model.forecast.chartLabels = data.data.timestamps;
                model.forecast.chartData = [ data.data.averageTemperatures, data.data.windSpeeds];

            }, function(error){

                // TODO

                model.forecast.error = error;

            }).finally(function(){

                // Hide the spinner (even after an error)
                model.forecast.isLoadingForecast = false;
            });

            // Show the modal forecast dialog
            modal.show('forecastDialog');
        }
        /**
         * Generic initialization of the forecast chart
         */
        function initChart() {

            model.forecast.chartLabels = [];
            model.forecast.chartData = [ [], [] ];

            model.forecast.chartDatasetOverride = [
                {
                    yAxisID: 'y-axis-1',
                    lineTension: 0,
                    borderWidth: 1,
                    fill: false,
                    borderColor: 'blue'
                },
                {
                    yAxisID: 'y-axis-2',
                    lineTension: 0,
                    borderWidth: 1,
                    fill: false,
                    borderColor: 'green'
                }
            ];

            var displayFormat = 'MMM-DD HH:mm';

            model.forecast.chartOptions = {
                animation: false,
                maintainAspectRatio: true,
                responsive: true,
                elements: {point: {radius: 3}},
                scales: {
                    xAxes: [
                        {
                            ticks: {fontSize: 12},
                            type: 'time',
                            gridLines: {
                                display: false
                            },
                            time: {
                                displayFormats: {
                                    'millisecond': displayFormat,
                                    'second': displayFormat,
                                    'minute': displayFormat,
                                    'hour': displayFormat,
                                    'day': displayFormat,
                                    'week': displayFormat,
                                    'month': displayFormat,
                                    'quarter': displayFormat,
                                    'year': displayFormat,
                                },
                            },
                        }
                    ],
                    yAxes: [
                        {
                            id: 'y-axis-1',
                            type: 'linear',
                            ticks: {fontSize: 12},
                            display: true,
                            position: 'left',
                            gridLines: {
                                display: false
                            },
                            scaleLabel: {
                                display: true,
                                fontSize: 12,
                                labelString: '',
                                fontColor: 'blue'
                            }
                        },
                        {
                            id: 'y-axis-2',
                            type: 'linear',
                            ticks: {fontSize: 12},
                            display: true,
                            position: 'right',
                            gridLines: {
                                display: false
                            },
                            scaleLabel: {
                                display: true,
                                fontSize: 12,
                                labelString: '',
                                fontColor: 'green'
                            }
                        }
                    ]
                }
            };
        }

        // General initialization of a chart
        initChart();

        // Load the initial static data and then the weather
        loadInitialWeather();
    }

}(angular.module('backbase')));

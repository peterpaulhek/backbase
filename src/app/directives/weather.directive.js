/*
 Module      : weather.directive.js
 Description : This directive is the implementation of the <weather> tag to display weather
 History     : 2017-07-06 PPH Created
 */

'use strict';

(function (module) {

    module.directive('weather', weatherDirective);

    function weatherDirective() {

        var scope = {
            // @ local scope property is used to access string values that are defined outside the directive, e.g name="{{name}}"
            // = create a two-way binding between the outer scope and the directive’s isolate scope, e.g. person="person"
            // & allows the consumer of a directive to pass in a function that the directive can invoke, e.g. action="click()"
            name: '@',
            keyValuePairs: '=',
            keyColumns: '@',
            valueColumns: '@',
            error: '@',
            isLoading: '@',
            onRefresh: '&',
            onRemove: '&',
            onClick: '&',
        };

        var directive = {
            templateUrl: 'app/directives/weather.html',
            link: link,
            scope: scope,
            restrict: 'E'	// 'E' - only matches element name <directiveName></<directiveName>, C = Class, A = Attribute
        };

        function link(scope) {

            // Nothing to do here but leave it for clarity
        }

        return directive;
    }

}(angular.module('backbase')));

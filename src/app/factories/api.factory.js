/*
 Module      : api.factory.js
 Description : A wrapper for the Open Weather Data API
 History     : 2017-07-06 PPH Created
 */

'use strict';

(function (module) {

    module.factory('API', apiFactory);

    function apiFactory($q, Config, REST) {

        // The possible display units
        var displayUnits = {
            standard: 0,
            metric: 1,
            imperial: 2
        };

        // The current display units and defaults to metric
        var displayUnit = displayUnits.metric;

        // The current number of decimals to display
        var displayPrecision = 1;

        /**
         * Gets the current selected display unit
         */
        function getDisplayUnit() {
            return displayUnit;
        }

        /**
         * Sets a new display unit
         * Not implemented in this exercise is the check on a valid newDisplayUnit argument!
         */
        function setDisplayUnit(newDisplayUnit) {
            displayUnit = newDisplayUnit;
        }

        /**
         * Gets the current selected display precision
         */
        function getDisplayPrecision() {
            return displayPrecision;
        }

        /**
         * Sets a new display precision
         * Not implemented in this exercise is the check on a valid newDisplayPrecision argument!
         */
        function setDisplayPrecision(newDisplayPrecision) {
            displayPrecision = newDisplayPrecision;
        }

        /**
         * Returns the cities in the initial main overview.
         */
        function getCities() {

            var cities = [];

            angular.forEach(Config.cities.split(','), function(city){
                cities.push(city);
            });

            return cities;
        }

        /**
         * Returns the display unit of temperature
         * Not implemented in this exercise is the check on a valid displayUnit argument!
         */
        function getDisplayUnitOfTemperature(displayUnit) {

            var displayUnitOfTemperature = '';

            switch (displayUnit) {
                case displayUnits.standard:
                    displayUnitOfTemperature = 'K';
                    break;
                case displayUnits.metric:
                    displayUnitOfTemperature = '°C';
                    break;
                case displayUnits.imperial:
                    displayUnitOfTemperature = '°F';
                    break;
            }

            return displayUnitOfTemperature;
        }

        /**
         * Returns the temparature to display
         * Not implemented in this exercise is the check on a valid displayUnit arguments!
         */
        function getTemparatureToDisplay(temparature, displayUnit, displayPrecision) {
            return temparature.toFixed(displayPrecision) + ' ' + getDisplayUnitOfTemperature(displayUnit);
        }

        /**
         * Returns the display unit of wind
         * Not implemented in this exercise is the check on a valid displayUnit argument!
         */
        function getDisplayUnitOfWind(displayUnit) {

            var displayUnitOfWind = '';

            switch (displayUnit) {
                case displayUnits.standard:
                case displayUnits.metric:
                    displayUnitOfWind = 'm/s';
                    break;
                case displayUnits.imperial:
                    displayUnitOfWind = 'm/h';
                    break;
            }

            return displayUnitOfWind;
        }

        /**
         * Returns the wind to display
         * Not implemented in this exercise is the check on a valid displayUnit arguments!
         */
        function getWindToDisplay(wind, displayUnit, displayPrecision) {
            return wind.toFixed(displayPrecision) + ' ' + getDisplayUnitOfWind(displayUnit);
        }

        /**
         * Returns the request parameter '&units=' (empty for the standard display unit)
         */
        function getDisplayUnitToRequest(displayUnit) {

            var displayUnitToRequest = '';

            switch (displayUnit) {
                case displayUnits.metric:
                    displayUnitToRequest = '&units=metric';
                    break;
                case displayUnits.imperial:
                    displayUnitToRequest = '&units=imperial';
                    break;
            }

            return displayUnitToRequest;
        }

        /**
         * Returns the weather for a specific city.
         * This method should actually accept a list of weather characteristics to be queried so that this API factory
         * does not need to be changed if the app needs additional/different weather characteristics.
         */
        function getWeatherByName(city) {

            // Get a deferred object from the $q service
            var deferred = $q.defer();

            // Construct the url
            var url = Config.base + Config.weather + '?' + Config.appid + '&q=';

            // Query the Open Weather Data API
            REST.get(url + city + getDisplayUnitToRequest(displayUnit)).then(function(data) {
                deferred.resolve({
                    averageTemparature: getTemparatureToDisplay(data.main.temp, displayUnit, displayPrecision),
                    windSpeed: getWindToDisplay(data.wind.speed, displayUnit, displayPrecision)
                });
            }, function(error) {
                deferred.reject(error);
            });

            // Return the promise and let the caller wait for resolve() or reject()
            return deferred.promise;
        }

        /**
         * Returns the forecast for a specific city.
         * This method should actually accept a list of weather characteristics to be queried so that this API factory
         * does not need to be changed if the app needs additional/different weather characteristics.
         */
        function getForecastByName(city) {

            // Get a deferred object from the $q service
            var deferred = $q.defer();

            // Construct the url
            var url = Config.base + Config.forecast + '?' + Config.appid + '&q=';

            // Query the Open Weather Data API
            REST.get(url + city + getDisplayUnitToRequest(displayUnit)).then(function(data) {

                // Recreate the list in a different format
                var list = {
                    timestamps: [],
                    averageTemperatures: [],
                    windSpeeds: []
                };

                // Iterate the received list
                angular.forEach(data.list, function(timestamp){

                    // Add it to the new lists; convert the unix time to a moment time
                    list.timestamps.push(moment.unix(timestamp.dt));
                    list.averageTemperatures.push(timestamp.main.temp);
                    list.windSpeeds.push(timestamp.wind.speed);
                });

                deferred.resolve({
                    data: list,
                    displayUnitOfTemperature: getDisplayUnitOfTemperature(displayUnit),
                    displayUnitOfWind: getDisplayUnitOfWind(displayUnit)
                });
            }, function(error) {
                deferred.reject(error);
            });

            // Return the promise and let the caller wait for resolve() or reject()
            return deferred.promise;
        }

        return {
            displayUnits: displayUnits,
            getDisplayUnit: getDisplayUnit,
            setDisplayUnit: setDisplayUnit,
            getDisplayPrecision: getDisplayPrecision,
            setDisplayPrecision: setDisplayPrecision,
            getCities: getCities,
            getWeatherByName: getWeatherByName,
            getForecastByName: getForecastByName
        };
    }

}(angular.module('backbase')));

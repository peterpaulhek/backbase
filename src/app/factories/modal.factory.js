/*
 Module      : modal.factory.js
 Description : A simple wrapper for a modal dialog
               Main purpose is to hide a modal dialog when navigating in the browser (backward, forward)
 History     : 2017-07-06 PPH Created
 */

'use strict';

(function (module){

    module.factory('Modal', modalFactory);

    function modalFactory($timeout, $window) {

        function Modal($scope) {

            var that = this;

            this.show = show;
            this.hide = hide;

            this.isExiting = false;

            /**
             * Show a modal dialog.
             */
            function show(name) {
                that.name = name;
                $('#' + name).appendTo('body').modal();
            }

            /**
             * Hide the modal dialog.
             */
            function hide() {
                $('#' + that.name).modal('hide');
            }

            /**
             * Main purpose of the factory: hide it when navigation is started.
             */
            $scope.$on('$locationChangeStart', function(event, next){

                if (that.isExiting) { // Double check we are not exiting
                    // do nothing
                } else {

                    event.preventDefault(); // Cancel navigation for now

                    $('.modal').modal('hide'); // Hide the dialog

                    $timeout(function() {
                        $window.location.href = next; // The actual navigation is deferred 500 ms.
                    }, 500);

                    that.isExiting = true; // Do not call ourselves recursively
                }
            });
        }

        return {
            Modal: Modal
        };
    }

}(angular.module('backbase')));

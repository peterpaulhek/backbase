/*
 Module      : rest.factory.js
 Description : A wrapper for the $http service methods get()
 History     : 2017-07-06 PPH Created
 */
'use strict';

(function (module) {

    module.factory('REST', restFactory);

    function restFactory($http, $q) {

        /**
         * Wrapper for the get() method of the $http service.
         */
        function httpGet(uri) {

            // Get a deferred object from the $q service
            var deferred = $q.defer();

            var config = {cache: false}; // Disable caching

            $http.get(uri, config)
                .success(function(data /*, status, headers, config */) {
                    deferred.resolve(data); // Mark as resolved
                })
                .error(function(/*data, status, headers, config*/) {
                    deferred.reject('Sorry, the weather information is not available'); // Mark as rejected
                });

            // Return the promise and let the caller wait for resolve() or reject()
            return deferred.promise;
        }

        return {
            get: httpGet
        };
    }

}(angular.module('backbase')));
